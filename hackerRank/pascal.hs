module Main where

pascal n = (concat $ printPascal)
  where
  pascalList = map calcPascVal rowIndex
  rowIndex = zip [0..n-1] (map makeList [0..n-1])
  makeList m = [0..m]
  calcPascVal (i,xs) = calcPascVal'' (i,xs) 
  printPascal =   (map (++"\n") (map (concat) (map (map (++" ")) ((map(map(show)) pascalList)))))

fact 0 = 1
fact 1 = 1
fact n = n * (fact (n-1)) 

calcPascVal'' (n,rLst) = map (calcPascVal') rLst
	where
	calcPascVal' r = (fact n) `div` ((fact r) * (fact(n-r)))
    -- Enter your code here to complete this function


-- This part is related to the Input/Output and can be used as it is
-- Do not modify it
main = do
    input <- getLine
    putStrLn . pascal . (read :: String -> Int) $ input
