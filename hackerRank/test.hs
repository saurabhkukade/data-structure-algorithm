module Main where

getList :: Int -> IO [String]
getList n = if n==0 then return [] else do i <- getLine; is <- getList(n-1); return (i:is)
nextMove :: Int -> [String] -> String
main = do

    n <- getLine
--    xy <- getLine

   let i = read n
   grid <- getList i

   putStrLn.nextMove i $ grid
