module Main where
-- isPanagram :: String -> String

isPanagram ls | length(ls) < 26 = "not Panagram" 
	      | otherwise = checkForAllAlpha ls

checkForAllAlpha ls = if (length (filter (==False) boolAlphaSet)) == 0 then "panagram" else "not panagram"
		where
		asciStr = map fromEnum ls
		onlyAlphaRange = filter (\x -> (x > 96 && x < 123) ||  (x > 64  && x < 91)) asciStr
		sameLevelRange = map (add32) onlyAlphaRange
		boolAlphaSet = map (verifyAlpha) [97,98..122]
		verifyAlpha x = if (length (filter (==x) sameLevelRange)) > 0 then True else False

add32 x  = if (x > 64 && x < 91) then (x+32) else x

main = do
    grid <- getLine 
    putStrLn.isPanagram $ grid

