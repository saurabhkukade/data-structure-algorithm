#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
int findEle(int A[],int key,int pos,int size);
int findEle(int A[],int key,int pos,int size)
{
    int i,j=0;
    for(i=0;i<size;i++){
        if(A[i]>=key){
            j++;
        } 
        if (j==pos)
            return A[i];
    }
    return 0;
}
int main()
{
	
    int i,size,query,key,val;
    scanf("%d",&size);
    scanf("%d",&query);
    int arr[100000];
    int res[100000];
    for(i=0;i<size;i++)
        scanf("%d",&arr[i]);

    for(i=0;i<query;i++){
        scanf("%d",&key);
        scanf("%d",&val);
        res[i] = findEle(arr,key,val,size);
    }
    for(i=0;i<query;i++)
        printf("%d\n",res[i]);
    return 0;
}
