#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
int parsed=0;
typedef struct list{
    int *elements;
    int num;
    int size;
}List;

int findEle(int A[],int key,int pos,int size,List *storage);
int findEle(int A[],int key,int pos,int size,List *storage)
{
    int i,j=0;
    if(parsed!=0){
        for(i=0;i<parsed;i++)
            if(storage[i].num==key)
                return storage[i].elements[pos-1];
    }

    parsed++;
    storage[parsed-1].num = key;
    storage[parsed-1].size= 0;
    storage[parsed-1].elements = NULL;

        
    for(i=0;i<size;i++){
        if(*(A+i)>=key){
            storage[parsed-1].size++;
            storage[parsed-1].elements = (int *)realloc(storage[parsed-1].elements,(sizeof(int)*size));
            storage[parsed-1].elements[storage[parsed-1].size -1] = *(A+i);
            j++;
        } 
    }
    
    return storage[parsed-1].elements[pos-1];
}
int main()
{
    
	
    int i,size,query,*arr,key,val,*res;
    List *storage;
    scanf("%d",&size);
    scanf("%d",&query);
    arr = (int *)malloc(sizeof(int)*size);
    storage = (List *)malloc(sizeof(List)*size);
    res = (int *)malloc(sizeof(int)*query);
    for(i=0;i<size;i++)
        scanf("%d",(arr+i));
    
    for(i=0;i<query;i++){
        scanf("%d",&key);
        scanf("%d",&val);
        res[i] = findEle(arr,key,val,size,storage);
    }

    for(i=0;i<query;i++)
        printf("%d\n",*(res+i));
    return 0;
}

