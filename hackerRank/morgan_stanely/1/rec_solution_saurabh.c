#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int maxiSum(int stocksArr[],int size);
int recMaxiSum(int stocksArr[],int size,int index)
{
    int i,max,subtraction1,subtraction2,temp,sum=0;
    int half=((size)/2);
    for(i=index;i<size;i++){
        if(i>0 && i < size-1){                  //checking i betn 0 and last
            if( (stocksArr[i-1]%2==0) && (stocksArr[i+1] % 2 == 0) ){
                //subtraction1 = abs(stocksArr[i] - stocksArr[(size-i-1)]);
                temp = ((stocksArr[i-1]) + (stocksArr[i+1]))/2;
                //subtraction2 = abs(temp - stocksArr[(size-i-1)]);
                //if(subtraction2 > subtraction1)
                stocksArr[i] = temp;
            }
        }
    }
    for(i=0;i<size/2;i++){
        sum +=abs(stocksArr[i]-stocksArr[size-i-1]);
    }
    return sum;

}


int maxiSum(int stocksArr[],int size)
{
    int i,changed,subtraction1,subtraction2,temp,sum=0;
    int half=((size)/2);
    for(i=0;i<size;i++){
        if(i>0 && i < size-1){                  //checking i betn 0 and last
            if( (stocksArr[i-1]%2==0) && (stocksArr[i+1] % 2 == 0) ){
                subtraction1 = recMaxiSum(stocksArr,size,i+1);
                temp = ((stocksArr[i-1]) + (stocksArr[i+1]))/2;
                changed = stocksArr[i]; 
                stocksArr[i] = temp;
                subtraction2 = recMaxiSum(stocksArr,size,i+1);
                if(subtraction1>subtraction2)
                    stocksArr[i] = changed;
            }
        }
    }
    for(i=0;i<size/2;i++){
        sum +=abs(stocksArr[i]-stocksArr[size-i-1]);
    }
    if((size)%2!=0)
        sum = sum + stocksArr[i];

    return sum;

}
int main() {

    int testCases,numStocks;
    int *stocksArr,*result;
    int i,j;

    scanf("%d",&testCases);
    result = (int *)malloc(sizeof(int)*testCases); 

    for(i=0;i<testCases;i++){
        scanf("%d",&numStocks); //getting number of stocks
        stocksArr = (int *)malloc(sizeof(int)*numStocks); //allocating memory to stocks array
        if(stocksArr==NULL){
            printf("ram full");
            return 0;
        }
        for(j=0;j<numStocks;j++) //getting stocks in stockArr
            scanf("%d",&stocksArr[j]);

        result[i] = maxiSum(stocksArr,numStocks);
        free(stocksArr);
    }

    for(i=0;i<testCases;i++)
        printf("%d\n",result[i]);

    return 0;
}
