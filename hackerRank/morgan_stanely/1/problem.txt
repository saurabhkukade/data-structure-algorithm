

Problem Statement

Hari owns N stocks of N companies, one from each company. The value of ith stock is Pi. He got a chance to change the values of N−2 stocks except for first (i=1) and last (i=N) stocks.

He can change the value of ith stock to (Pi−1+Pi+12), if both the values Pi−1 and Pi+1 are even. He can change the value of any stock only one time. If jth stock value is changed after changing ith stock value then j must be greater than i.

Help Hari to change the stock values so that he can maximize the following:
S=∑i=1⌊N2⌋|Pi−PN−i+1|
Find the possible maximum value of S.

Input Format

The first line of input is an integer T, total number of test cases. Each test case consists of two lines, the first line is the integer N, the total number of stocks, and the second line has N space separated integers, which are the stock values Pi.

Constraints

    1≤T≤5×103
    3≤N≤20
    1≤Pi≤104

Output Format

Output T lines, the maximum possible value of S per line for each test case.

Sample Input

2
4
2 4 3 2
4
2 4 3 8

Sample Output

1
8

Explanation

    [2, 4, 3, 2]: P3 is odd, so the value of P2 can not be changed. P3 can be changed to (P2+P42), which equals to 3, so changing the value does not affect the initial stock values. S = 1.
    [2, 4, 3, 8]: Here P3 can be changed to 6, so new stock values become [2, 4, 6, 8] and S = 8

