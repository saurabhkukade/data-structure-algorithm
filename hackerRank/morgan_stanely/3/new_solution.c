#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
typedef struct list{
    int cost;
    int tNum;
    int *set;
    struct list *next;
    struct list *prev;
}node;

int mergeSets(node **head,int x,int y)
{
    int i,j,k,l=0;
    node *temp,*temp2;
    temp=*head;
    while(temp!=NULL){
        for(i=0;i<temp->tNum;i++)

            if(x == temp->set[i]){ //checking for x
                //for(j=0;j<temp->tNum;j++)
                //if(y == temp->set[j]) //checking for y
                //return 0;
                //else{

                        temp2=*head;
                         while(temp2!=NULL){
                         for(j=0;j<temp2->tNum;j++)
                             if(y==temp2->set[j]){

                                 temp->set = realloc(temp->set,sizeof(int)*(temp->tNum+temp2->tNum));
                                 for(k=temp->tNum;k<(temp->tNum+temp2->tNum);k++){
                                     temp->set[k] = temp2->set[l];
                                     l++;
                                 }
                                 temp->tNum = temp->tNum+temp2->tNum;
                                 temp->cost = temp->cost+temp2->cost;
                                 if(temp->next==temp2){ //if very next node
                                     temp->next = temp2->next;
                                     if(temp2->next!=NULL)
                                         temp2->next->prev = temp;
                                     else
                                         temp->next=NULL;
                                     return 0;
                                 }
                                 if((temp2->prev!=NULL)&&(temp2->next!=NULL)){
                                         temp2->next->prev=temp2->prev;
                                         temp2->prev->next=temp2->next;
                                 }
                                 //free(temp2);
                                 return 0;
                             }
                         temp2 = temp2->next;
                         }
                         //}
            }
        temp=temp->next;
    }
    return 1;
}


int createSet(node **head,int x,int y,int xCost, int yCost) 
{
    int i,j,k,l;
    node *new,*temp,*temp2,*parentY,*temp3;
    if((*head)==NULL)
    {
        (*head) = (node *)malloc(sizeof(node));
        (*head)->tNum = 2;
        (*head)->set=(int *)malloc(sizeof(int)*2);
        (*head)->set[0] = x;
        (*head)->set[1] = y;
        (*head)->cost = xCost+yCost;
        (*head)->next=NULL;
        (*head)->prev=NULL;
        return 0;
    }
    temp=(*head);
    temp3=(*head);
    if(xCost == 0 && yCost == 0){
        while(temp3!=NULL){
            for(k=0;k<temp3->tNum;k++)
            {
                if( x == temp3->set[k] ){
                    for(k=0;k<temp3->tNum;k++)
                        if(y==temp3->set[k]){
                            return 0;
                        }
                }
            }
            temp3=temp3->next;
        mergeSets(head,x,y);
         return 0;
        }
    }
    while(temp->next!=NULL){
        for(i=0;i<temp->tNum;i++){
            if (x == temp->set[i]){
                temp->tNum++;
                temp->set=realloc(temp->set,sizeof(node)*(temp->tNum));
                temp->set[temp->tNum-1] = y;
                temp->cost = temp->cost+yCost;
                return 0;
            }
        }
        temp = temp->next;
    }
    (new) = (node *)malloc(sizeof(node));
    (new)->tNum = 2;
    (new)->set=(int *)malloc(sizeof(int)*2);
    (new)->set[0] = x;
    (new)->set[1] = y;
    (new)->cost = xCost+yCost;
    (new)->next=NULL;
    (new)->prev=temp;
    temp->next = new;
    return 0;
}


int findMinCost(int A[],node *head,int size)
{
    int min=10001,i;
    for(i=0;i<size;i++){
        if (A[i]!=0)
            if(min > A[i])
                min = A[i];
    }
    while(head!=NULL){
        if(min > head->cost)
            min = head->cost;
        head = head->next;
    }
    return min;
}

int display(node *head)
{
    int i;
    while(head!=NULL){
        printf("\n");
        for(i=0;i<head->tNum;i++)
            printf("%d\t",head->set[i]);
        printf("\t cotst = %d\n",head->cost);
    printf("\n");
        head=head->next;
    }
    return 0;
}


int main() 
{
    int i,j,x,y,minCost;
    int size,query,*res,*A;
    node *head=NULL;
    scanf("%d",&size);
    scanf("%d",&query);
    A = (int *)malloc(sizeof(int)*size);
    res = (int *)malloc(sizeof(int)*query);

    for(i=0;i<size;i++)
        scanf("%d",&A[i]);


    for(i=0;i<query;i++){
        scanf("%d%d",&x,&y);
        createSet(&head,x,y,A[x-1],A[y-1]);
        A[x-1]=0;
        A[y-1]=0;
        res[i] = findMinCost(A,head,size);
    }

    for(i=0;i<query;i++)
        printf("%d\n",res[i]);

    return 0;
}

