#include<stdio.h>
#include<stdlib.h>

typedef struct Gtree{
	int data;
	struct Gtree **arrChild;
	int numChild;
}gTree;

int insert(gTree **rootNode,int parent, int data);
int insert(gTree **rootNode,int parent, int data)
{
	gTree *newNode;
	int i;
	if(*rootNode == NULL){
		(*rootNode) = (gTree *)malloc(sizeof(gTree));
		(*rootNode) -> data = parent;
		(*rootNode) ->arrChild = NULL;
		(*rootNode) -> numChild = 0;
		return 0;
	}

	if((*rootNode) -> data == parent){
		// allocating memory new Node 
		newNode  = (gTree *)malloc(sizeof(gTree));
		newNode -> data = data;
		newNode->arrChild = NULL; 
		newNode -> numChild  = 0;

		//attaching child to parent 

		(*rootNode) -> numChild = (*rootNode) -> numChild + 1;
		(*rootNode) -> arrChild = (gTree **)realloc((*rootNode)->arrChild,sizeof(gTree *)*((*rootNode)->numChild));
		(*rootNode) -> arrChild[((*rootNode)->numChild)-1] = newNode;
		return 0;
	}

	//if data is not equal to parent then giving recursive call to all of its child
	
	for(i = 0; i<(((*rootNode)->numChild)-1); i++)
		insert(&((*rootNode)->arrChild[i]),parent,data);

	return 0;
}

int displayGTree(gTree *rootNode)
{
	int i,totalChild;
	if(rootNode==NULL)
		return -1;
	printf("root = %d\n",rootNode->data);
	totalChild = rootNode->numChild;
	for(i=0;i<totalChild;i++)
		printf("childs = %d\t",rootNode->arrChild[i]->data);
	printf("\n");
	for(i=0;i<totalChild;i++)
		displayGTree(rootNode->arrChild[i]);
	return 0;
}



int main(int argc, char **argv)
{
	int i,j,x,y;
    int testCases,nCities,nSpells,nMult;
    int *costCities;
    int *res;

    scanf("%d",&testCases);
    res = (int *)malloc(sizeof(int)*testCases);
    for(i=0;i<testCases;i++){
        scanf("%d",&nCities);
        scanf("%d",&nMult);
        scanf("%d",&nSpells);
        costCities = (int *)malloc((sizeof(int)*nCities));
        for(j=0;j<nCities;j++)
            scanf("%d",&costCities[i]);
        gTree *rootNode = NULL;

        for(j=0;j<nCities-1;j++){
            scanf("%d%d",&x,&y);
            insert(&rootNode,x,y);
        }
        displayGTree(rootNode);
        free(costCities);
    }

	return 0;
}
