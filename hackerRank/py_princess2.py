#!/bin/python


def calcPath(x,y):
    vertMove = [] 
    horzMove = []
    x1 = x[0]-y[0]
    y1 = x[1]-y[1]
    if (x1 > 0):
            return("DOWN")
    if (x1 < 0):
            return("UP")
    if (y1 > 0):
            return("RIGHT")
    if (y1 < 0):
            return("LEFT")
    return("")

def nextMove(n,r,c,grid):
    mGrid = []
    findM = 0
    findP = 0
    for x in grid:
        for y in x:
             mGrid.append(y) 
    grid = ''.join(mGrid) 
    findP =  grid.index('p')
    mat=[]
    for x in range(0,n):
        for y in range(0,n):
            mat.append((x,y))

    for x in mat:
        if(x[0] == r and x[1] == c):
            findM = x
    
    return calcPath(mat[findP],findM)















n = input()
r,c = [int(i) for i in raw_input().strip().split()]
grid = []
for i in xrange(0, n):
    grid.append(raw_input())

print nextMove(n,r,c,grid)
