module Main where
getList :: Int -> IO[String]
getList n = if n==0 then return [] else do i <- getLine; is <- getList(n-1); return (i:is)
displayPathtoPrincess :: Int -> [String] -> String

displayPathtoPrincess m ls = (concat $ map (++"\n") $ (calcPath findM findP))
	where
	mat = [(x,y)| x <- [0..m-1], y <- [0..m-1]]
	findM = mat !! ((length (takeWhile (/='m') $ (concat ls))))
	findP = mat !! ((length (takeWhile (/='p') $ (concat ls))))

calcPath (x1,y1) (x2,y2) = (vertMoves (x1-x2)) ++ (horzMoves (y1+y2)) 

vertMoves x | x > 0 = take (x) $ repeat $ "UP"
	    | x < 0 = take (abs(x)) $ repeat $ "DOWN"
	    | otherwise = []	  
	
horzMoves x | x > 0 = take (x) $ repeat $ "LEFT"
	    | x < 0 = take (abs(x)) $ repeat $ "RIGHT"
	    | otherwise = []	  
main = do
    n <- getLine
    let i = read n
    grid <- getList i
    putStrLn.displayPathtoPrincess i $ grid

