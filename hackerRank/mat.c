#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <inttypes.h>

int main() {
    
    int i,j,n,fDig=0,sDig=0;
    scanf("%d",&n);
    int mat[n][n];
    for(i=0;i<n;i++)
        for(j=0;j<n;j++)
            scanf("%d",&mat[i][j]);
    for(i=0;i<n;i++)
        fDig += mat[i][i];
    for(i=n-1,j=0;i>=0,j<n;i--,j++)
	    sDig += mat[j][i];
    printf("%d",abs(fDig-sDig));    
    

    /* Enter your code here. Read input from STDIN. Print output to STDOUT */    
    return 0;
}
