#include<stdio.h>
#include<stdlib.h>

typedef struct btree{
	int data;
	struct btree *left,*right;
}bTree;

typedef struct stack{
	int data;
	int capacity;
	int top;
	struct stack *next;
}STACK;


int insertBtree(bTree **root,int data);
int displayTree(bTree *root);
int createStack(STACK **s,int maxSize);
int push(STACK **s,int data);
int pop(STACK **s);
int getTop(STACK *s);
int isOverFlow(STACK *s);
int isUnderFlow(STACK *s);


int createStack(STACK **s,int maxSize)
{
	if(*s != NULL)
		return -1;
	*s = (STACK *)malloc(sizeof(STACK)); //creating stack and allocating memory
	(*s) -> capacity = maxSize;            // giving maximum size of elements which stacks can have.
	(*s) -> top = 0; 			     // setting top positon at level 0
	return 0;
}

int push(STACK **s,int data)
{
	STACK *newItem,*tempItem;
	if(isOverFlow((*s))==0){
		printf("overFlow\n");
		return -1;
	}
	if((*s)->next==NULL){
		newItem = (STACK *)malloc(sizeof(STACK));
		newItem -> data = data;
		newItem -> capacity = (*s)->capacity;
		newItem -> top = ((*s) -> top) +1;
		(*s) -> next = newItem;
		return 0;
	}
	tempItem = *s;
	while(tempItem->next!=NULL)
		tempItem = tempItem->next;

	newItem = (STACK *)malloc(sizeof(STACK));
	newItem -> data = data;
	newItem -> capacity = tempItem->capacity;
	newItem -> top = (tempItem -> top)+1;
	tempItem -> next = newItem;
	return 0;
}

int pop(STACK **s)
{
	STACK *tempItem,*lastNode;
	int data;
	tempItem = *s;
	if(isUnderFlow(*s)==0){
		printf("UnderFlow\n");
		return -1;
	}
	while(tempItem->next!=NULL){
		lastNode = tempItem;
		tempItem = tempItem->next;
	}
	data = tempItem->data;
	lastNode->next=NULL;
	free(tempItem);  //check whether it will be null or garbage

	return data;
}

int isOverFlow(STACK *s)
{
	while(s->next!=NULL)
		s = s->next;
	if(s->capacity==s->top)
		return 0;
	return 1;
}

int isUnderFlow(STACK *s)
{
	while(s->next!=NULL)
		s = s->next;
	if(s->top==0)
		return 0;
	return 1;
}

int getTop(STACK *s){

	if(s->next==NULL)
		return 0;
	while(s->next!=NULL)
		s=s->next;
	return (s->data);
}

int displayStack(STACK *s)
{
	if(s->next==NULL)
		return 0;
	s = s->next;
	while(s!=NULL){
		printf("%d\t",s->data);
		s = s->next;
	}
}


int insertBtree(bTree **root,int data)
{
	if(*root==NULL){
		*root = (bTree *)malloc(sizeof(bTree));
		(*root)->data = data;
		(*root)->left=NULL;
		(*root)->right=NULL;
		return 0;
	}
	if(data < (*root)->data)
		insertBtree(&(*root)->left,data);
	if(data > (*root)->data)
		insertBtree(&(*root)->right,data);
	return 0;
}

int displayTree(bTree *root)
{
	if(root==NULL)
		return 0;
	printf("data = %d \n",root->data);
	displayTree(root->left);
	displayTree(root->right);
	return 0;
}

int printAnscestors(bTree *root,int element,STACK **s)
{
	if(root==NULL)
		return -1;
	if(root->data > element){
		if(printAnscestors(root->left,element,&(*s)) == 0){
			push(&(*s),root->data);
			return 0;
		}
		else
			return -1;
	}
	else if(root->data < element){
		if(printAnscestors(root->right,element,&(*s)) == 0){
			push(&(*s),root->data);
			return 0;
		}
		else
			return -1;
	}
	else{
			push(&(*s),root->data);
		return 0;

	}

	return 0;
}
int main(int argc, char **argv)
{
	int i;
	bTree *root = NULL;
	STACK *s = NULL;
	createStack(&s,10);
	for(i=1;i<argc-1;i++)
		insertBtree(&root,atoi(argv[i]));
	printAnscestors(root,atoi(argv[argc-1]),&s);
	displayStack(s);
	printf("\n");
	return 0;
}

