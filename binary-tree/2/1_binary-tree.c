#include<stdio.h>
#include<stdlib.h>

typedef struct btree{
	int data;
	struct btree *right;
	struct btree *left;
}bTree;

typedef struct list{
	int data;
	struct list *next;
}node;
int insertList(node **head,int val);
int displayList(node *head);
int insertBtree(bTree **root,int data);
int displayBtree(bTree *root);
int heightOfTree(bTree *root);
int diameterOfSubTree(bTree *root,node **head);
int findDiameter(node *head);

int insertList(node **head,int val)
{
	node *tempNode, *newNode;
	if(*head == NULL){
		(*head) = (node *)malloc(sizeof(node));
		(*head) -> data = val; 
		(*head) -> next = NULL;
		return 0;
	}
	tempNode = *head;
	while(tempNode->next != NULL)
		tempNode = tempNode->next;
	newNode = (node *) malloc(sizeof(node));
	newNode -> data = val;
	newNode -> next = NULL;
	tempNode -> next = newNode;
	return 0;
}

// function to display the linked list
int displayList(node *head)
{
	if(head==NULL)
		return 0;
	while(head!=NULL){
		printf("data = %d\n",head->data);
		head = head->next;
	}
	return 0;
}
//function to insert elements into binary Tree
int insertBtree(bTree **root,int data)
{
	bTree *tempNode;
	if (*root == NULL){
		(*root) = (bTree *) malloc (sizeof(bTree));
		(*root) -> data = data; 
		(*root) -> right = NULL;
		(*root) -> left = NULL;
		return 0;
	}
	tempNode = *root;
	if (data > (tempNode) -> data)
		insertBtree(&(tempNode)->right,data);
	else if(data < (tempNode) -> data)
		insertBtree(&(tempNode)->left,data);
	else
		return 0;
	return 0;
}
// first order traversal of binaray Tree
int displayBtree(bTree *root)
{
	if(root == NULL)
		return 0;
	printf("data = %d  ",root->data);
	displayBtree(root->left);
	displayBtree(root->right);
	return 0;
}

//fun to calculate height of binary tree
int heightOfTree(bTree *root)
{
	int lheight,rheight;
	if(root == NULL)
		return 0;
	lheight = heightOfTree(root->left);
	rheight = heightOfTree(root->right);
	if (lheight > rheight)
		return(lheight+1);
	else 
		return(rheight+1);
}

//function to find the dimeter of sub tree
int diameterOfSubTree(bTree *root,node **head)
{
	int dim;
	if(root==NULL)
		return 0;
	dim = heightOfTree(root->left)+heightOfTree(root->right);
	insertList(&(*head),dim);
	diameterOfSubTree(root->left,(&(*head)));
	diameterOfSubTree(root->right,(&(*head)));
	return 0;
	
}

// function to find the maximum diameter of subTree
int findDiameter(node *head)
{
	int max = 0;
	if(head==NULL)
		return 0;
	while(head!=NULL){
		if(head->data > max)
			max=head->data;
		head=head->next;
	}
	printf("diameter = %d\n",max+1);
	return 0;
}

int main(int argc, char **argv)
{
	int i;
	bTree *root = NULL;
	node *head  = NULL;
	for(i=1;i<argc;i++)
		insertBtree(&root,atoi(argv[i]));
	diameterOfSubTree(root,&head);
	findDiameter(head);
	return 0;
}

