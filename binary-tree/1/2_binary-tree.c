//convert given binary tree into its mirror
//mirror of the tree is another tree with left and right children of all non-leaf nodes interchanged

#include<stdio.h>
#include<stdlib.h>

typedef struct btree{
	int data;
	struct btree *right;
	struct btree *left;
}bTree;

typedef struct list{
	int level;
	int data;
	struct list *next;
}node;

//function to insert elements into binary Tree
int insertBtree(bTree **root,int data)
{
	bTree *tempNode;
	if (*root == NULL){
		(*root) = (bTree *) malloc (sizeof(bTree));
		(*root) -> data = data; 
		(*root) -> right = NULL;
		(*root) -> left = NULL;
		return 0;
	}
	tempNode = *root;
	if (data > (tempNode) -> data)
		insertBtree(&(tempNode)->right,data);
	else if(data < (tempNode) -> data)
		insertBtree(&(tempNode)->left,data);
	else
		return 0;
	return 0;
}
// functions to change the binary tree into mirror binary tree
int convertIntoMirrorTree(bTree **root)
{
	bTree *tempRoot;
	if(*root==NULL)
		return 0;
	if((((*root)->left)==NULL) && ((((*root)->right)) == NULL))
		return 0;

	if((*root)->left!=NULL){
		if((*root)->right==NULL){
			(*root)->right = (*root)->left;
			free((*root)->left);
		}
	}
	if((*root)->right!=NULL){
		if((*root)->left==NULL){
			(*root)->left = (*root)->right;
			free((*root)->right);
		}
	}
	if((((*root)->left)!=NULL) && ((((*root)->right)) != NULL)){
		tempRoot=(*root)->left;
		(*root)->left = (*root)->right;
		(*root)->right = tempRoot;
	}
	convertIntoMirrorTree(&(*root)->left);
	convertIntoMirrorTree(&(*root)->right);
}
	
// first order traversal of binaray Tree
int displayBtree(bTree *root)
{
	if(root == NULL)
		return 0;
	printf("data = %d  ",root->data);
	displayBtree(root->left);
	displayBtree(root->right);
}

int main(int argc, char **argv)
{
	int i,height;
	bTree *root = NULL;
	node *head = NULL;
	for(i=1;i<argc;i++)
		insertBtree(&root,atoi(argv[i]));
	printf("before converting \n\t");
	displayBtree(root);
	printf("\n");
	printf("after converting \n\t");
	convertIntoMirrorTree(&root);
	displayBtree(root);
	return 0;
}
