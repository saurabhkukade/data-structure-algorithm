#include<stdio.h>
#include<stdlib.h>

typedef struct btree{
	int data;
	struct btree *right;
	struct btree *left;
}bTree;

typedef struct list{
	int level;
	int data;
	struct list *prev;
	struct list *next;
}node;

//function to insert elements into binary Tree
int insertBtree(bTree **root,int data)
{
	bTree *tempNode;
	if (*root == NULL){
		(*root) = (bTree *) malloc (sizeof(bTree));
		(*root) -> data = data; 
		(*root) -> right = NULL;
		(*root) -> left = NULL;
		return 0;
	}
	tempNode = *root;
	if (data > (tempNode) -> data)
		insertBtree(&(tempNode)->right,data);
	else if(data < (tempNode) -> data)
		insertBtree(&(tempNode)->left,data);
	else
		return 0;
	return 0;
}
// first order traversal of binaray Tree
int displayBtree(bTree *root)
{
	if(root == NULL)
		return 0;
	printf("data = %d  ",root->data);
	displayBtree(root->left);
	displayBtree(root->right);
}
// function to insert into LinkList
int insertList(node **head,bTree *root,int level)
{
	node *tempNode, *newNode;
	if(*head == NULL){
		(*head) = (node *)malloc(sizeof(node));
		(*head) -> level = level;
		(*head) -> data = (root)->data;
		(*head) -> prev = NULL;
		(*head) -> next = NULL;
		return 0;
	}
	tempNode = *head;
	while(tempNode->next != NULL){
		tempNode = tempNode->next;
	}
	newNode = (node *) malloc(sizeof(node));
	newNode -> level = level;
	newNode -> data = (root -> data);
	newNode -> prev = tempNode;
	newNode -> next = NULL;
	tempNode -> next = newNode;
	return 0;
}
// function to display Linked list
int displayList(node *head)
{
	if (head == NULL)
		return 0;
	while(head!=NULL){
		printf("data = %d level = %d \n",head->data,head->level);
		head = head -> next;
	}
	return 0;
}
// function to add elements of tree level-wise into a linked list
int insertToListFromTree(node **head,bTree *root,int level)
{
	if (root == NULL)
		return 0;
	insertList(&(*head),root,level);
	if(root->left!=NULL)
		insertToListFromTree(&(*head),root->left,(level+1));
	if(root->right!=NULL)
		insertToListFromTree(&(*head),root->right,(level+1));
}
//fun to calculate height of binary tree
int heightOfTree(bTree *root)
{
	int lheight,rheight;
	if(root == NULL)
		return 0;
	lheight = heightOfTree(root->left);
	rheight = heightOfTree(root->right);
	if (lheight > rheight)
		return(lheight+1);
	else 
		return(rheight+1);
}
		
//function to find the level of maximum sum in binary tree
int doLevelSum(node *head,int level)
{
	int i,j,temp,sum,*levelSum,max=0,onLevel;
	node *tempNode;
	levelSum = (int *)malloc((sizeof(int)*level));
	for(i=0;i<level;i++){
		sum=0;
		tempNode = head;
		while(tempNode!=NULL){
			if(i==(tempNode->level))
					sum += tempNode->data;
			tempNode = tempNode->next;
		}
		levelSum[i]=sum;
	}

	for(i=0;i<level;i++){
		if (max < levelSum[i]){
			max = levelSum[i];
			onLevel = i;
		}
	}
	printf("\nLEVEL = %d has  SUM = %d",onLevel,max);
	printf("\nlevel is starting from 1 to n\n\n");
	
}

int printZigZag(node *head,int height)
{
	node *tempHead, *lastNode,*temp;
	int i,j,count=0;
	tempHead=head;
	while(tempHead->next!=NULL){
		tempHead = tempHead->next;
		count++;
	}
	count++;
	lastNode =tempHead;
	for(i=0;i<height;i++){
		if(i % 2==0){
			temp = head;
			while(temp!=NULL){
				if(i== temp->level){
					printf("\neven\n");
					printf("%d,",temp->data);
				}
				temp = temp -> next;
			}
		}
		else{
			temp = lastNode;
			while(temp!=NULL)
				if(temp->level==i){
					printf("%d",temp->data);
				temp = temp->prev;
				}
		}
	}
	return 0;
}

int main(int argc, char **argv)
{
	int i,height;
	bTree *root = NULL;
	node *head = NULL;
	for(i=1;i<argc;i++)
		insertBtree(&root,atoi(argv[i]));
	insertToListFromTree(&head,root,0);
	height = heightOfTree(root);
	//doLevelSum(head,height);
	printZigZag(head,height);
	return 0;
}
