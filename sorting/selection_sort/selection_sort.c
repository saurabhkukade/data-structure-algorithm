#include<stdio.h>
#include<stdlib.h>

int selectionSort(int A[],int size);
int selectionSort(int A[],int size)
{
	int i,j;
	int min,temp,index;
	for(i=0;i<size;i++){
		min=i;
		for(j=i+1;j<size;j++)
			if (A[min] > A[j]){
				min = j;
			}
		temp = A[min];
		A[min] = A[i];
		A[i] = temp;
	}
}

int main(int argc, char **argv)
{
	int *numArr;
	int i;
	numArr = (int *)malloc(sizeof(int)*argc-1);
	for(i=1;i<argc;i++)
		numArr[i-1] = atoi(argv[i]);
	selectionSort(numArr,argc-2);
	for(i=0;i<argc-2;i++)
		printf("%d\t",numArr[i]);
	return 0;
}
