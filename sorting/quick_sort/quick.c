#include<stdio.h>
#include<stdlib.h>

int quickSort(int A[],int low,int high);
int partition(int A[],int low,int high);
int partition(int A[],int low,int high)
{
	int pivot = A[low];
	int left = low;
	int right = high;
	int temp;

	while(left < right){
		while(pivot >= A[left])
			left++;
		while(pivot < A[right])
			right--;
		if(left < right){
			temp = A[left];
			A[left] = A[right];
			A[right] = temp;
		}
	}
	A[low] = A[right];
	A[right] = pivot;
	return right;
}


int quickSort(int A[],int low,int high)
{
	printf("high = %d\n",high);
	int pivot;
	if(high>low){
		pivot = partition(A,low,high);
		quickSort(A,low,pivot-1);
		quickSort(A,pivot+1,high);
	}
	return 0;
}


int main(int argc, char **argv)
{
	int *numArr;
	int i;
	numArr = (int *)malloc(sizeof(int)*argc-1);
	for(i=1;i<argc;i++)
		numArr[i-1] = atoi(argv[i]);
	quickSort(numArr,0,argc-2);
	for(i=0;i<argc-1;i++)
		printf("%d\t",numArr[i]);
	return 0;
}
