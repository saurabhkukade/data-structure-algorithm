#include<stdio.h>
#include<stdlib.h>

int heapSort(int A[],int size);
int buildHeap(int A[],int size);
int heapify(int A[],int size,int index);


int heapify(int A[],int size,int index)
{
	int left,right,temp,max;
	left = (2*index)+1;
	right = (2*index)+2;
	if((left < size) && (right <= size)){
		max = (A[left] > A[right] ? left : right);
		if(A[index] > A[max]) {
			temp = A[index];
			A[index] = A[max];
			A[max] = temp;
		}
	}
	return 0;
	

}
int buildHeap(int A[],int size)
{
	int i,left,right,max,temp;
	for(i=(size)/2;i>=0;i--){
		left = (2*i)+1;
		right = (2*i)+2;
		if((left < size) && (right <= size)){
			max = (A[left] > A[right] ? left : right);
			if(A[i] > A[max]) {
				temp = A[i];
				A[i] = A[max];
				A[max] = temp;
				heapify(A,size,max);
			}
		}
	}
}
int heapSort(int A[],int size)
{
	buildHeap(A,size);
	int i,temp;
	for(i=size-1;i>0;i--){
		temp = A[i];
		A[i] = A[0];
		A[0] = temp;
		buildHeap(A,i);
	}
	return 0;
}
		
int main(int argc, char **argv)
{
	int *numArr;
	int i;
	numArr = (int *)malloc(sizeof(int)*argc);
	for(i=1;i<argc;i++)
		numArr[i-1] = atoi(argv[i]);
	heapSort(numArr,argc-1);
	for(i=0;i<argc-1;i++)
		printf("%d\t",numArr[i]);
	return 0;
}

