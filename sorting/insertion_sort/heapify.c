#include<stdio.h>
#include<stdlib.h>
int heapify(int A[],int size);

int heapify(int A[],int size)
{
	if(size <= 1)
		return 0;
	int i,j,maxOfTwo,temp; 
	for (j=0;j<=(size/2)-1;j++){
		for (i=0;i<=(size/2)-1;i++){
			maxOfTwo = (A[(2*i)+1] > A[(2*i)+2] ? (2*i)+1 : (2*i)+2);
			if(A[i] < A[maxOfTwo]){
				temp = A[maxOfTwo];
				A[maxOfTwo] = A[i];
				A[i] = temp;
			}
		}
	}
	return 0;
}

int main(int argc, char **argv)
{
	int *numArr;
	int i;
	numArr = (int *)malloc(sizeof(int)*argc);
	for(i=1;i<argc;i++)
		numArr[i-1] = atoi(argv[i]);
	heapify(numArr,argc-1);
	for(i=0;i<argc-1;i++)
		printf("%d\t",numArr[i]);
	return 0;
}
