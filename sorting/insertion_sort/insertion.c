#include<stdio.h>
#include<stdlib.h>
int insertion_sort(int numArr[],int size);

int insertion_sort(int numArr[],int size)
{
	int i,j,temp;
	for(i=1;i<size;i++){
		temp = numArr[i];
		j= i-1;
		while(j>=0 && numArr[j] > temp){
			numArr[j+1] = numArr[j];
			j--;
		}
		numArr[j+1] = temp;
	}
	return 0;
}

int main(int argc, char **argv)
{
	int *numArr;
	int i;
	numArr = (int *)malloc(sizeof(int)*argc);
	for(i=1;i<argc;i++)
		numArr[i-1] = atoi(argv[i]);
	insertion_sort(numArr,argc-1);
	for(i=0;i<argc-1;i++)
		printf("%d\t",numArr[i]);
	return 0;
}

//10 12 13 11 15
