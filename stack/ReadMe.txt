Author = Saurabh kukade

Content:
	This (stackOp.c) have basic stack operations:

	1. push
	2. pop
	3. isUnderFlow
	4. isOverFlow
	5. getTop

How to compile :

	gcc stackOp.c

How to Run :

	./a.out <integer elements> < MaxSize Of stack>

	example:
	
	./a.out 1 2 3 4 5 6 10

	10 -> will be the maxSize of stack
