#include<stdio.h>
#include<stdlib.h>
typedef struct stack{
	int data;
	int capacity;
	int top;
	struct stack *next;
}STACK;

int createStack(STACK **s,int maxSize);
int push(STACK **s,int data);
int pop(STACK **s);
int getTop(STACK *s);
int isOverFlow(STACK *s);
int isUnderFlow(STACK *s);

int createStack(STACK **s,int maxSize)
{
	if(*s != NULL)
		return -1;
	*s = (STACK *)malloc(sizeof(STACK)); //creating stack and allocating memory
	(*s) -> capacity = maxSize;            // giving maximum size of elements which stacks can have.
	(*s) -> top = 0; 			     // setting top positon at level 0
	return 0;
}

int push(STACK **s,int data)
{
	STACK *newItem,*tempItem;
	if(isOverFlow((*s))==0){
		printf("overFlow\n");
		return -1;
	}
	if((*s)->next==NULL){
		newItem = (STACK *)malloc(sizeof(STACK));
		newItem -> data = data;
		newItem -> capacity = (*s)->capacity;
		newItem -> top = ((*s) -> top) +1;
		(*s) -> next = newItem;
		return 0;
	}
	tempItem = *s;
	while(tempItem->next!=NULL)
		tempItem = tempItem->next;

	newItem = (STACK *)malloc(sizeof(STACK));
	newItem -> data = data;
	newItem -> capacity = tempItem->capacity;
	newItem -> top = (tempItem -> top)+1;
	tempItem -> next = newItem;
	return 0;
}

int pop(STACK **s)
{
	STACK *tempItem,*lastNode;
	int data;
	tempItem = *s;
	if(isUnderFlow(*s)==0){
		printf("UnderFlow\n");
		return -1;
	}
	while(tempItem->next!=NULL){
		lastNode = tempItem;
		tempItem = tempItem->next;
	}
	data = tempItem->data;
	lastNode->next=NULL;
	free(tempItem);  //check whether it will be null or garbage

	return data;
}

int isOverFlow(STACK *s)
{
	while(s->next!=NULL)
		s = s->next;
	if(s->capacity==s->top)
		return 0;
	return 1;
}

int isUnderFlow(STACK *s)
{
	while(s->next!=NULL)
		s = s->next;
	if(s->top==0)
		return 0;
	return 1;
}

int getTop(STACK *s){

	if(s->next==NULL)
		return 0;
	while(s->next!=NULL)
		s=s->next;
	return (s->data);
}
int displayStack(STACK *s)
{
	if(s->next==NULL)
		return 0;
	s = s->next;
	while(s!=NULL){
		printf("%d\t",s->data);
		s = s->next;
	}
}


int main(int argc, char **argv)
{
	int i;
	STACK *s = NULL;
	createStack(&s,atoi(argv[argc-1]));
	for(i=1;i<argc-1;i++){
		push(&s,atoi(argv[i]));
	}
	displayStack(s);
	return 0;
}
