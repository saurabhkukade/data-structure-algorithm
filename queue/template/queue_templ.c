#include<stdio.h>
#include<stdlib.h>

typedef struct Queue{
    int data;
    struct Queue *next;
}queue;

int insertQueue(queue **head,int data);
int deleteQueue(queue **head);
int printQueue(queue *head);

int insertQueue(queue **head,int data)
{
    queue *new,*tempHead;
    new = (queue *)malloc(sizeof(queue));
    if(new==NULL)
        return 1;
    new->data = data;
    new->next = NULL;
    if(*head == NULL){
        *head = new;
        return 0;
    }
    new->next = *head;
    *head  =  new;
    return 0;
}

int deleteQueue(queue **head)
{
    queue *tempHead,*prevLast;
    if(*head == NULL)
        return 0;
    tempHead = *head;
    while(tempHead->next!=NULL){
        prevLast = tempHead;
        tempHead = tempHead->next;
    }
    free(tempHead);
    prevLast->next = NULL;
    return 0;
}

int printQueue(queue *head)
{
    if(head==NULL)
        return 0;

    while(head!=NULL){
        printf("%d\t",head->data);
        head = head->next;
    }
    printf("\n");
    return 0;
}

int main(int argc, char **argv)
{
    queue *head=NULL;
    int i;
    for(i=1;i<argc;i++)
        insertQueue(&head,atoi(argv[i]));
    printQueue(head);
    deleteQueue(&head);
    printQueue(head);
    return 0;
}



