//Author =  saurabh kukade
#include<stdio.h>
#include<stdlib.h>
typedef struct tree{
	int data;
	int num;
	struct tree *left;
	struct tree *right;
}Tree;
int createTree(Tree **root,int value);
int insertIntoTree(Tree **root,int fromNode,int toNode,int toVal);
int checkForUnivalued(Tree *root);
int isUniValued(Tree *root,int val);
int total = 0;

int createTree(Tree **root,int value)
{
	if(*root==NULL){
		(*root) = (Tree *)malloc(sizeof(Tree));
		(*root) -> data = value;
		(*root) -> num = 0;
		(*root) -> right = NULL;
		(*root) -> left = NULL;
		return 0;
	}
	return 1;
}

int insertIntoTree(Tree **root,int fromNode,int toNode,int toVal)
{
	Tree *newNode;
	if(*root==NULL)
		return 0;
	if((*root)->num!=fromNode){
		insertIntoTree(&((*root)->left),fromNode,toNode,toVal);
		insertIntoTree(&((*root)->right),fromNode,toNode,toVal);
	}
	if((*root)->num==fromNode){
		if(((*root)->left!=NULL) && ((*root)->right!=NULL))
			return 1;
		newNode = (Tree *)malloc(sizeof(Tree));
		newNode -> data = toVal;
		newNode -> num = toNode;
		if((*root)->left == NULL)
			(*root)->left = newNode;
		else
			(*root)->right = newNode;
		return 0;
	}
	return 1;
}
int checkForUnivalued(Tree *root)
{
	int flag;
	if(root==NULL)
		return 0;
	flag = isUniValued(root,root->data);
	if(flag == 0)
		total++;
	checkForUnivalued(root->left);
	checkForUnivalued(root->right);
	return 0;

}
int isUniValued(Tree *root,int val)
{
	if(root == NULL)
		return 0;
	if(root->left!=NULL){
		if(root->left->data == val)
			(isUniValued(root->left,val));
		else 
			return 1;
	}
	if(root->right!=NULL){
		if(root->right->data == val)
			(isUniValued(root->right,val));
		else
			return 1;
	}
	return 0;
}
		
int main()
{
	int i,n,*value,*fromNode,*toNode;
	Tree *root = NULL;
	scanf("%d",&n);
	value = (int *)malloc(sizeof(int)*n);
	fromNode = (int *)malloc(sizeof(int)*(n-1));
	toNode = (int *)malloc(sizeof(int)*(n-1));
	for(i=0;i<n;i++)
		scanf("%d",&(value[i]));
	createTree(&root,value[0]);
	for(i=0;i<n-1;i++){
		scanf("%d",&(fromNode[i]));
		scanf("%d",&(toNode[i]));
	}
	for(i=0;i<n-1;i++)
		insertIntoTree(&root,fromNode[i],toNode[i],value[toNode[i]]);
	checkForUnivalued(root);
	printf("%d\n",total);
	return 0;
}
