#include<stdio.h>
#include<stdlib.h>

typedef struct Gtree{
	int data;
	struct Gtree **arrChild;
	int numChild;
}gTree;

int insert(gTree **rootNode,int parent, int data);
int deleteLeaf (gTree **rootNode,int data);
int insert(gTree **rootNode,int parent, int data)
{
	gTree *newNode;
	int i;
	if(*rootNode == NULL){
		(*rootNode) = (gTree *)malloc(sizeof(gTree));
		(*rootNode) -> data = data;
		(*rootNode) ->arrChild = NULL;
		(*rootNode) -> numChild = 0;
		return 0;
	}

	if((*rootNode) -> data == parent){
		// allocating memory new Node 
		newNode  = (gTree *)malloc(sizeof(gTree));
		newNode -> data = data;
		newNode->arrChild = NULL; 
		newNode -> numChild  = 0;

		//attaching child to parent 

		(*rootNode) -> numChild = (*rootNode) -> numChild + 1;
		(*rootNode) -> arrChild = (gTree **)realloc((*rootNode)->arrChild,sizeof(gTree *)*((*rootNode)->numChild));
		(*rootNode) -> arrChild[((*rootNode)->numChild)-1] = newNode;
		return 0;
	}

	//if data is not equal to parent then giving recursive call to all of its child
	
	for(i = 0; i<(((*rootNode)->numChild)-1); i++)
		insert(&((*rootNode)->arrChild[i]),parent,data);

	return 0;
}

int displayGTree(gTree *rootNode)
{
	int i,totalChild;
	if(rootNode==NULL)
		return -1;
	printf("root = %d\n",rootNode->data);
	totalChild = rootNode->numChild;
	for(i=0;i<totalChild;i++)
		printf("childs = %d\t",rootNode->arrChild[i]->data);
	printf("\n");
	for(i=0;i<totalChild;i++)
		displayGTree(rootNode->arrChild[i]);
	return 0;
}

int deleteLeaf (gTree **rootNode,int data)
{
	int i,j,flag = 0;
	if(*rootNode == NULL)
		return -1;

}



int main(int argc, char **argv)
{
	int i;
	gTree *rootNode = NULL;
	insert(&rootNode,1,10);
	insert(&rootNode,10,11);
	insert(&rootNode,10,12);
	insert(&rootNode,11,20);
	displayGTree(rootNode);
	printf("\nAfter Deletion\n");
	deleteLeaf(&rootNode,10);
	printf("\nAfter Deletion\n");
	displayGTree(rootNode);
	deleteLeaf(&rootNode,12);

	return 0;
}
